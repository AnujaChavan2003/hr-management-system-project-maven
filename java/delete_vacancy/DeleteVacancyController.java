package delete_vacancy;

import candidate_dashboard.CandidateDashboard;
import db_operation.DbUtil;
import delete_user.DeleteUser;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import user_dashboard.UserDashboard;
import vacancy_dashboard.VacancyDashboard;


public class DeleteVacancyController {


	@FXML
	private Button admin;
	@FXML
	private Button candidate;
	@FXML
	private Button vacancy;
	@FXML
	private Button deleted;
	@FXML
	private Button cancel;
	@FXML
	private TextField hManager;
	
	public void Admin(ActionEvent event) {
		new UserDashboard().Show();
	}
	public void Deleted(ActionEvent event) {
	System.out.println(hManager.getText());
	if (hManager.getText().isEmpty()) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText(null);
        alert.setContentText("Please fill the Field.");
        alert.showAndWait();
        return;
	}
	String query ="delete from vacancy where hManager='"+hManager.getText()+"';";
	System.out.println(query);
	DbUtil.executeQuery(query);
	System.out.println("Event occur delete controller ");
    new VacancyDashboard().Show();
	}
	
	public void Cancel(ActionEvent event) {
		new VacancyDashboard().Show();
	}
	public void Candidate(ActionEvent event) {
		new CandidateDashboard().Show();
	}
	public void Vacancy(ActionEvent event) {
		new VacancyDashboard().Show();
	}
	
}
