package candidate_dashboard;


import add_candidate.AddCandidate;
import delete_candidate.DeleteCandidate;
import edit_candidate.EditCandidate;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import search_candidate.SearchCandidate;
import user_dashboard.UserDashboard;
import vacancy_dashboard.VacancyDashboard;

public class CandidateDashboardController {


	@FXML
	private Button candidate;
	@FXML
	private Button search;
	@FXML
	private Button delete;
	@FXML
	private Button edit;
	@FXML
	private Button vacancy;
	@FXML
	private Button admin;
	@FXML
	private Button add;
	
	
	
	public void Add(ActionEvent event) {
		new AddCandidate().Show();
	}

	public void Search(ActionEvent event) {
		new SearchCandidate().Show();
	}

	public void Edit(ActionEvent event) {
		new EditCandidate().Show();
	}
	public void Candidate(ActionEvent event) {
		new CandidateDashboard().Show();
	}
	public void Vacancy(ActionEvent event) {
		new VacancyDashboard().Show();
	}
	public void Admin(ActionEvent event) {
		new UserDashboard().Show();
	}
	public void Delete(ActionEvent event) {
		new DeleteCandidate().Show();
	}


	
	
	
}
