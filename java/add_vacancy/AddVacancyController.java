package add_vacancy;

import java.net.URL;
import java.util.ResourceBundle;

import add_candidate.AddCandidate;
import add_user.AddUser;
import candidate_dashboard.CandidateDashboard;
import dashboard.Dashboard;
import db_operation.DbUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import search.Search;
import vacancy_dashboard.VacancyDashboard;

public class AddVacancyController implements Initializable {

	@FXML
	private TextField Vacancy;
	@FXML
	private ChoiceBox<String> jobTitle;
	private String[] select = {"Chief Executive Officer", "Database Administrator","HR Manager","IT Manager","QA Lead","Social Media Marketer","Software Architect","Software Engineer"};
	String[] Select=select;
	
	@FXML
	private TextField hManager;
	@FXML
	private TextField position;
	@FXML
	private Button cancel;
	@FXML
	private Button save;
	@FXML
	private Button admin;
	@FXML
	private Button recruitment;
	@FXML
	private Button vacancies;
	@FXML
	private Button candidates;
	
	public void AddVacancy(ActionEvent event) {
		
	}
	public void admin(ActionEvent event) {
		new AddUser().Show();;
	}
	public void recruitment(ActionEvent event) {
		new Dashboard().Show();
	}
	public void Cancel(ActionEvent event) {
		new VacancyDashboard().Show();
	}
	public void Save(ActionEvent event) {
		System.out.println(Vacancy.getText());
		System.out.println(jobTitle.getValue());
		System.out.println(hManager.getText());
		System.out.println(position.getText());
		if (Vacancy.getText().isEmpty() || jobTitle.getValue().isEmpty() ||hManager.getText().isEmpty()|| position.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText(null);
            alert.setContentText("Please fill all the Fields.");
            alert.showAndWait();
            return;
		}
		
		String query="insert into Vacancy(Vacancy,jobTitle,hManager,Position) values('"+Vacancy.getText()+"','"+jobTitle.getValue()+"','"+hManager.getText()+"','"+position.getText()+"');\r\n"
				+ " ";
		System.out.println(query);
		DbUtil.executeQuery(query);
		System.out.println("Event occur save controller "+event.getEventType().getName());
		new VacancyDashboard().Show();
	}
	public void AddCandidate(ActionEvent event) {
		new AddCandidate().Show();
	}
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		jobTitle.getItems().addAll(select);

	}
	
	
	
	
}
