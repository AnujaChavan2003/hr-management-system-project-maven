package user_dashboard;

import add_user.AddUser;
import candidate_dashboard.CandidateDashboard;
import dashboard.Dashboard;
import delete_user.DeleteUser;
import edit_user.EditUser;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import search_user.SearchUser;
import vacancy_dashboard.VacancyDashboard;

public class UserDashboardController {

	@FXML
	private Button admin;
	@FXML
	private Button search;
	@FXML
	private Button delete;
	@FXML
	private Button edit;
	@FXML
	private Button candidate;
	@FXML
	private Button vacancy;
	@FXML
	private Button add;
	
	
	public void Admin(ActionEvent event) {
		new Dashboard().Show();
	}

	public void Add(ActionEvent event) {
		new AddUser().Show();
	}

	public void Delete(ActionEvent event) {
		new DeleteUser().Show();
	}

	public void Search(ActionEvent event) {
		new SearchUser().Show();
	}

	public void Edit(ActionEvent event) {
		new EditUser().Show();
	}
	public void Candidate(ActionEvent event) {
		new CandidateDashboard().Show();
	}
	public void Vacancy(ActionEvent event) {
		new VacancyDashboard().Show();
	}
	
	
	
}
