package search_candidate;

import javafx.beans.property.SimpleStringProperty;

public class User {

	public SimpleStringProperty fullName =new SimpleStringProperty();
	public SimpleStringProperty middleName=new SimpleStringProperty();
	public SimpleStringProperty lastName=new SimpleStringProperty();
	public SimpleStringProperty vacancy=new SimpleStringProperty();
	public SimpleStringProperty email=new SimpleStringProperty();
	public SimpleStringProperty contactNumber=new SimpleStringProperty();
	public SimpleStringProperty resume=new SimpleStringProperty();
	public SimpleStringProperty keywords=new SimpleStringProperty();

	

	  public String getFullName(){
		  return null!=fullName.get() ? fullName.get() : "";
	   }

	   public String getMiddleName(){
		   return null!=middleName.get() ? middleName.get() : "";
	   }

	   public String getLastName(){
		   return null!=lastName.get() ? lastName.get() : "";
	   }

	   public String getVacancy(){
		   return null!=vacancy.get() ? vacancy.get() : "";
	   }

	   public String getEmail(){
		   return null!=email.get() ? email.get() : "";
	   }
	   public String getContactNumber(){
		   return null!=contactNumber.get() ? contactNumber.get() : "";	
		   }
	   
	   public String getResume(){
		   return null!=resume.get() ? resume.get() : "";
	   }
	   public String getKeywoards(){
		   return null!=keywords.get() ? keywords.get() : "";
	   }

}
