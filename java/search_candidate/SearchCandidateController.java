package search_candidate;

import java.net.URL;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import candidate_dashboard.CandidateDashboard;
import db_operation.DbUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import user_dashboard.UserDashboard;
import vacancy_dashboard.VacancyDashboard;

public class SearchCandidateController implements Initializable {
	@FXML
	private TextField candidateToSearch;
	@FXML
	private Button cancel;
	@FXML
	private Button search;
	@FXML
	private Button admin;
	@FXML
	private Button candidate;
	@FXML
	private Button vacancy;
	@FXML
	private TableView<User> tableView;
	@FXML

	private TableColumn<User, String> column1;
	@FXML
	private TableColumn<User, String> column2;
	@FXML
	private TableColumn<User, String> column3;
	@FXML
	private TableColumn<User, String> column4;
	@FXML
	private TableColumn<User, String> column5;
	@FXML
	private TableColumn<User, String> column6;
	@FXML
	private TableColumn<User, String> column7;
	@FXML
	private TableColumn<User, String> column8;
	private ObservableList<User> data;


	public void Admin(ActionEvent event) {

		new UserDashboard().Show();

	}
	public void Candidate(ActionEvent event) {

		new CandidateDashboard().Show();

	}
	public void Cancel(ActionEvent event) {
		new CandidateDashboard().Show();

	}
	
	public void Vacancy(ActionEvent event) {
		new VacancyDashboard().Show();

	}

	
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		data = FXCollections.observableArrayList();

		column1.setCellValueFactory(new PropertyValueFactory<User, String>("fullName"));
		column2.setCellValueFactory(new PropertyValueFactory<User, String>("middleName"));
		column3.setCellValueFactory(new PropertyValueFactory<User, String>("lastName"));
		column4.setCellValueFactory(new PropertyValueFactory<User, String>("vacancy"));
		column5.setCellValueFactory(new PropertyValueFactory<User, String>("email"));
		column6.setCellValueFactory(new PropertyValueFactory<User, String>("contcatNo"));
		column7.setCellValueFactory(new PropertyValueFactory<User, String>("resume"));
		column8.setCellValueFactory(new PropertyValueFactory<User, String>("keywords"));


		buildData();

		FilteredList<User> filteredData = new FilteredList<>(data, b -> true);
	     candidateToSearch.textProperty().addListener((observable, oldValue, newValue) -> {
			filteredData.setPredicate(User -> {
				if (newValue.isEmpty() || newValue.isBlank() || newValue == null) {
					return true;
				}
				String searchKeyword = newValue.toLowerCase();
				if (User.getFullName().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else if (User.getMiddleName().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else if (User.getLastName().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else if (User.getVacancy().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else if (User.getEmail().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else if (User.getResume().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				}else if (User.getKeywoards().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;}
				else if (User.getContactNumber().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;}
				else
					return false;

			});
		});

		SortedList<User> sortedData = new SortedList<>(filteredData);

		sortedData.comparatorProperty().bind(tableView.comparatorProperty());
		tableView.setItems(sortedData);

	}

	public void buildData() {
		try {
			data = FXCollections.observableArrayList();
			
			String query = "Select *from candidates";
			System.out.println(query);
			ResultSet resultSet = DbUtil.executeQueryGetResult(query);
			while (resultSet.next()) {
				User user = new User();
				user.fullName.set(resultSet.getString(1));
				user.middleName.set(resultSet.getString(2));
				user.lastName.set(resultSet.getString(3));
				user.vacancy.set(resultSet.getString(4));
				user.resume.set(resultSet.getString(5));
				user.contactNumber.set(resultSet.getString(6));
				user.keywords.set(resultSet.getString(7));
				user.email.set(resultSet.getString(8));



				data.add(user);
			}
			tableView.setItems(data);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}