package delete_candidate;

import candidate_dashboard.CandidateDashboard;
import db_operation.DbUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import vacancy_dashboard.VacancyDashboard;

public class DeleteCandidateController {

	
		@FXML
		private Button admin;
		@FXML
		private Button candidate;
		@FXML
		private Button vacancy;
		@FXML
		private Button cancel;
		@FXML
		private Button deleted;
		@FXML
		private TextField fname;
		
		public void admin(ActionEvent event) {
			new CandidateDashboard().Show();
		}
		public void candidate(ActionEvent event) {
			new CandidateDashboard().Show();
		}
		public void vacancy(ActionEvent event) {
			new VacancyDashboard().Show();
		}
		public void cancel(ActionEvent event) {
			new CandidateDashboard().Show();
		}
		public void deleted(ActionEvent event) {
			System.out.println(fname.getText());
			if (fname.getText().isEmpty()) {
	            Alert alert = new Alert(Alert.AlertType.ERROR);
	            alert.setTitle("Error");
	            alert.setHeaderText(null);
	            alert.setContentText("Please fill the Field.");
	            alert.showAndWait();
	            return;
			}		
			String query="delete from candidates where FirstName='"+fname.getText()+"';";
			System.out.println(query);
			DbUtil.executeQuery(query);
			System.out.println("Event occur delete controller ");
		    new CandidateDashboard().Show();
			
		}
	}



