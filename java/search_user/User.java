package search_user;

import javafx.beans.property.SimpleStringProperty;


public class User {
	
	    public SimpleStringProperty Id=new SimpleStringProperty(); //class provides full implemetation of wrapping of string value
		public SimpleStringProperty userRole=new SimpleStringProperty(); //class provides full implemetation of wrapping of string value
		public SimpleStringProperty employeeName=new SimpleStringProperty();
		public SimpleStringProperty status=new SimpleStringProperty();
		public SimpleStringProperty userName=new SimpleStringProperty();
		public SimpleStringProperty password=new SimpleStringProperty();
		public SimpleStringProperty confirmPassword=new SimpleStringProperty();
		

		public String getId(){
			  return null!=Id.get() ? Id.get() : "";
		   }
		  public String getUserRole(){
			  return null!=userRole.get() ? userRole.get() : "";
		   }

		   public String getEmployeeName(){
			  return null!=employeeName.get() ? employeeName.get() : "";
		   }

		   public String getStatus(){
			   return null!=status.get() ? status.get() : "";		   }

		   public String getUserName(){
			   return null!=userName.get() ? userName.get() : "";		   }

		   public String getPassword(){
			   return null!=password.get() ? password.get() : "";
		   } 
		   public String getConfirmPassword(){
			   return null!=confirmPassword.get() ? confirmPassword.get() : "";
		   }
		  
	}
	

