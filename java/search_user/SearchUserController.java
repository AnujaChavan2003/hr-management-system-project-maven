package search_user;

import java.net.URL;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import candidate_dashboard.CandidateDashboard;
import dashboard.Dashboard;
import db_operation.DbUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import user_dashboard.UserDashboard;
import vacancy_dashboard.VacancyDashboard;


public class SearchUserController implements Initializable {
	@FXML
	private TextField adminToSearch;
	@FXML
	private Button cancel;
	@FXML
	private Button search;
	@FXML
	private Button admin;
	@FXML
	private Button vacancy;
	@FXML
	private Button candidate;
	
	@FXML
	private TableView<User> TableView;
	@FXML

	private TableColumn<User, String> column1; //TableColumn= datatype
	@FXML
	private TableColumn<User, String> column2;
	@FXML
	private TableColumn<User, String> column3;
	@FXML
	private TableColumn<User, String> column4;
	@FXML
	private TableColumn<User, String> column5;
	@FXML
	private TableColumn<User, String> column6;
	@FXML
	private TableColumn<User, String> column7;
	private ObservableList<User> data;

	public void Admin(ActionEvent event) {
		new UserDashboard().Show();
	}
	public void Cancel(ActionEvent event) {
		new Dashboard().Show();
	}

	public void Candidate(ActionEvent event) {
		new CandidateDashboard().Show();

	}
	public void Vacancy(ActionEvent event) {
		new VacancyDashboard().Show();

	}
	

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		data = FXCollections.observableArrayList();
		//setCellValueFactory=used to set all cells within  tablecolumn
		 //propertyValueFactory=to extract value from tableview row using property name
		column1.setCellValueFactory(new PropertyValueFactory<User, String>("Id"));
		column2.setCellValueFactory(new PropertyValueFactory<User, String>("userRole"));
		column3.setCellValueFactory(new PropertyValueFactory<User, String>("employeeName"));
		column4.setCellValueFactory(new PropertyValueFactory<User, String>("status"));
		column5.setCellValueFactory(new PropertyValueFactory<User, String>("userName"));
		column6.setCellValueFactory(new PropertyValueFactory<User, String>("password"));
		column7.setCellValueFactory(new PropertyValueFactory<User, String>("confirmPassword"));

		buildData();

		FilteredList<User> filteredData = new FilteredList<>(data, b -> true);
		adminToSearch.textProperty().addListener((observable, oldValue, newValue) -> {
			filteredData.setPredicate(User -> {
				if (newValue.isEmpty() || newValue.isBlank() || newValue == null) {
					return true;
				}
				String searchKeyword = newValue.toLowerCase();
				 if (User.getId().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				}else if (User.getUserRole().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else if (User.getEmployeeName().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else if (User.getStatus().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else if (User.getUserName().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else if (User.getPassword().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else if (User.getConfirmPassword().toLowerCase().indexOf(searchKeyword) > -1) {
					return true;
				} else
					return false;

			});
		});

		SortedList<User> sortedData = new SortedList<>(filteredData);

		sortedData.comparatorProperty().bind(TableView.comparatorProperty());
		TableView.setItems(sortedData);

	}

	public void buildData() {
		try {
			data = FXCollections.observableArrayList();
			String query = "Select *from Users";
			System.out.println(query);
			ResultSet resultSet = DbUtil.executeQueryGetResult(query);
			while (resultSet.next()) {
				User user = new User();
				user.Id.set(resultSet.getString(1));
				user.userRole.set(resultSet.getString(2));
				user.employeeName.set(resultSet.getString(3));
				user.status.set(resultSet.getString(4));
				user.userName.set(resultSet.getString(5));
				user.password.set(resultSet.getString(6));
				user.confirmPassword.set(resultSet.getString(7));

				data.add(user);
			}
			TableView.setItems(data);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
