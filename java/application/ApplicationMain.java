package application;


import db_operation.DbUtil;
import javafx.application.Application;
import javafx.stage.Stage;
import login.Login;

import stage_master.StageMaster;

public class ApplicationMain extends Application {

	public static void main(String args[]) {
		DbUtil.createDbConnection();
		launch(args);
	}
	
	public void start(Stage primaryStage) {
		StageMaster.setStage(primaryStage);
		System.out.println("Application start");
		new Login().Show();
		System.out.println("Application stop");
	}
}
