package edit_user;

import java.net.URL;

import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import dashboard.Dashboard;
import dashboard_candidate_recruitment.CommonDashboard;
import db_operation.DbUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import search_user.User;
import user_dashboard.UserDashboard;
import vacancy_dashboard.VacancyDashboard;

public class EditUserController implements Initializable {

	
	static Statement stmt;
	static Connection con;
	@FXML
	private Button cancel;
	@FXML
	private ChoiceBox<String> userRole;
	private String[] select = {"Admin","ESS"};
	
	@FXML
	private ChoiceBox<String> status;
	private String[] select1 = {"Enabled","Disabled"};
	
	@FXML
	private Label myLabel;
	@FXML
	private TextField userName;
	@FXML
	private TextField empName;
	@FXML
	private TextField adminToUpdate;
	@FXML
	private TextField password;
	@FXML
	private TextField confirmpassword;
	@FXML
	private Button admin;
	@FXML
	private Button edited;
	@FXML
	private Button candidate;
	@FXML
	private Button vacancy ;

	
	
	public void Admin(ActionEvent event) {
		new EditUser().Show();
	}
	public void Vacancy(ActionEvent event) {
		new VacancyDashboard().Show();
	}
	public void Candidate(ActionEvent event) {
		new CommonDashboard().Show();
	}
   public void Edited(ActionEvent event) throws SQLException {
System.out.println(adminToUpdate.getText());
	   
	   System.out.println( userName.getText());
	 	System.out.println(userRole.getValue());
		System.out.println(status.getValue());
		System.out.println(empName.getText());
		System.out.println(password.getText());
		System.out.println(confirmpassword.getText());
	           // Check if user exists in database
	         String query1 ="SELECT count(*) FROM Users WHERE UserName ='"+adminToUpdate.getText()+"'; ";
	           ResultSet rs1 =DbUtil.executeQueryGetResult(query1);
	           rs1.next();
	           if(rs1.getInt(1)==1) {
	       		if ( userName.getText().isEmpty()||userRole.getValue().isEmpty()|| status.getValue().isEmpty()|| empName.getText().isEmpty() || password.getText().isEmpty()|| confirmpassword.getText().isEmpty()) {
	       			Alert alert = new Alert(Alert.AlertType.ERROR);
	                alert.setTitle("Error");
	                alert.setHeaderText(null);
	                alert.setContentText("Please fill all the Fields.");
	                alert.showAndWait();
	                return;
	    		}
		 		String query = "update Users set empName ='" +empName.getText() + "',userRole ='"
				+userRole.getValue() + "',status"+ " ='" + status.getValue() + "',UserName ='"
				+ userName.getText() + "',Password ='" + password.getText() + "',ConfirmPassword ='" + confirmpassword.getText() + "' where UserName='"
				+ adminToUpdate.getText()+"';";
	   System.out.println(query);
		DbUtil.executeQuery(query);
		System.out.println("Event occur Edit controller "+event.getEventType().getName());
		new UserDashboard().Show();
		 } else {
			 Alert alert = new Alert(Alert.AlertType.ERROR);
				alert.setTitle("Error");
				alert.setHeaderText(null);
				alert.setContentText("Fill correct User");
				alert.showAndWait();
				return;
         }
	           }
  public void Cancel(ActionEvent event) {
		new UserDashboard().Show();
    }
	
  @Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		userRole.getItems().addAll(select);
		status.getItems().addAll(select1);
			
	}
  public void ConPassword(ActionEvent event) {
		String Password = password.getText();
		String ConfirmPassword = confirmpassword.getText();
		if (Password.equals(ConfirmPassword)) {
			new Dashboard().Show();
		} else {
			System.out.println("Data saved Unsuccesful");
		}
  }
  
}
