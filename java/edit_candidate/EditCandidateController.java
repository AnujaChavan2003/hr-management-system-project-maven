package edit_candidate;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import candidate_dashboard.CandidateDashboard;
import db_operation.DbUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import user_dashboard.UserDashboard;
import vacancy_dashboard.VacancyDashboard;

public class EditCandidateController implements Initializable {

	@FXML
	private TextField candidateToUpdate;
	@FXML
	private TextField fname;
	@FXML
	private TextField lname;
	@FXML
	private TextField mname;
	@FXML
    private ComboBox<String> Vacancy;
   private String Select[]= {"Associative IT Manager","Junior Account Assistant","Payroll Administrator","Sales Representative","Senior QA Lead"};
	
	@FXML
	private TextField contactNo;
	@FXML
	private TextField email;
	@FXML
	private TextField resume;
	@FXML
	private TextField keywords;
	@FXML
	private TextField date;
	@FXML
	private Button save;
	@FXML
	private Button cancel;
	@FXML
	private Button admin;
	@FXML
	private Button candidate;
	@FXML
	private Button vacancy;
	
	
	public void cancel(ActionEvent event) {
		new CandidateDashboard().Show();
	}
	public void save(ActionEvent event) throws SQLException {
		System.out.println(candidateToUpdate.getText());
		System.out.println(fname.getText());
		System.out.println(mname.getText());
		System.out.println(lname.getText());
		System.out.println(Vacancy.getValue());
		System.out.println(contactNo.getText());
		System.out.println(email.getText());
		System.out.println(resume.getText());
		System.out.println(keywords.getText());
		System.out.println(date.getText());
		//System.out.println(consent.getText());
		if (candidateToUpdate.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText(null);
            alert.setContentText("Please fill all the Fields.");
            alert.showAndWait();
            return;
		}
		String query1 ="SELECT count(*) FROM candidates WHERE FirstName ='"+candidateToUpdate.getText()+"'; ";
        ResultSet rs1 =DbUtil.executeQueryGetResult(query1);
        rs1.next();
        if(rs1.getInt(1)==1) {
		String query = "update candidates set FirstName ='" + fname.getText() + "',MiddleName ='"
				+ mname.getText() + "',LastName"
						+ " ='" + lname.getText() + "',Vacancy ='"
				+ Vacancy.getValue() + "',Email ='" + email.getText() + "',ContactNo ='" + contactNo.getText() + "',Keywords ='" + keywords.getText() + "',Date='" +date.getText()+"' where FirstName='"
				+ candidateToUpdate.getText()+"';";
		System.out.println(query);
			DbUtil.executeQuery(query);
			System.out.println("Event occur Edit controller "+event.getEventType().getName());
		new CandidateDashboard().Show();
	}else {
		 Alert alert = new Alert(Alert.AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(null);
			alert.setContentText("Fill correct Candidate");
			alert.showAndWait();
			return;
	}
	}
	public void Admin(ActionEvent event) {
		new UserDashboard().Show();
	}
	
	public void Candidate(ActionEvent event) {
		new EditCandidate().Show();
	}
	public void Vacancy(ActionEvent event) {
		new VacancyDashboard().Show();
	}
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
	  Vacancy.getItems().addAll(Select);
	}
	
	
}
