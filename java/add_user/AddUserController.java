package add_user;




import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;
import add_candidate.AddCandidate;
import dashboard.Dashboard;
import db_operation.DbUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import user_dashboard.UserDashboard;


public class AddUserController implements Initializable {

	@FXML
	private Label myLabel;
	@FXML
	private TextField userName;  //fx:id
	
	@FXML
	private ChoiceBox<String> userRole;
	private String[] select = {"Admin","ESS"};
	
	@FXML
	private ChoiceBox<String> status;
	private String[] select1 = {"Enabled","Disabled"}; 
	
	@FXML
	private TextField empName;
	@FXML
	private PasswordField password;
	@FXML
	private PasswordField confirmpassword;
	@FXML
	private Button cancel;
	@FXML
	private Button save;
	@FXML
	private Button admin;   
	@FXML
	private Button recruitment;
	
	
	public void save(ActionEvent event) throws SQLException {
		System.out.println(userName.getText());
		System.out.println(userRole.getValue());
		System.out.println(status.getValue());
		System.out.println(empName.getText());
		System.out.println(password.getText());
		System.out.println(confirmpassword.getText());	
		if ( userName.getText().isEmpty()||userRole.getValue().isEmpty()|| status.getValue().isEmpty()|| empName.getText().isEmpty() || password.getText().isEmpty()|| confirmpassword.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText(null);
            alert.setContentText("Please fill all the Fields.");
            alert.showAndWait();
            return;
		}
		 String query1="select count(*) from users where userName='" +userName.getText() + "';";
		  ResultSet rs1=DbUtil.executeQueryGetResult(query1);
		  rs1.next();
		  if(rs1.getInt(1)==1) {
			  System.out.println("this userName is already present");
			  Alert alert = new Alert(Alert.AlertType.ERROR);
	          alert.setTitle("Error");
	          alert.setHeaderText(null);
	          alert.setContentText("please check userName this userName is already Present");
	          alert.showAndWait();
	          return;
		 
		  }else {
			  if(password.getText().equals(confirmpassword.getText())) {
				  String query = "insert into Users(UserRole,EmpName,Status,UserName,Password,ConfirmPassword) values ('"+userRole.getValue()+"','"+empName.getText()+"','"+status.getValue()+"','"+userName.getText()+"','"+password.getText()+"','"+confirmpassword.getText()+"'); ";
					System.out.println(query);
					DbUtil.executeQuery(query);
					System.out.println("Event occur admin controller " + event.getEventType().getName());	
					new UserDashboard().Show();
					} 
			         else {
				  System.out.println("Password does not Match");
				  Alert alert = new Alert(Alert.AlertType.ERROR);
		            alert.setTitle("Error");
		            alert.setHeaderText(null);
		            alert.setContentText("Please fill correct same Passwords.");
		            alert.showAndWait();
		            return;
				}
		  }
	}

	 public void cancel(ActionEvent event) {
			new UserDashboard().Show();
		}
	 public void Recruitment(ActionEvent event) {
			new AddCandidate().Show();
		}
	 public void AddAdmin(ActionEvent event) {
		
		}

		@Override
		public void initialize(URL arg0, ResourceBundle arg1) //url track info  about a click
		                                                      // contain locale-specific objects
		{ 
		   userRole.getItems().addAll(select);// adding value to list
			status.getItems().addAll(select1);
				
		}
	

	}






	
