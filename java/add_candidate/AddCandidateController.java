package add_candidate;


import java.net.URL;
import java.util.ResourceBundle;

import add_user.AddUser;
import add_vacancy.AddVacancy;
import candidate_dashboard.CandidateDashboard;
import dashboard.Dashboard;
import dashboard_candidate_recruitment.CommonDashboard;
import db_operation.DbUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

public class AddCandidateController implements Initializable
 {

	@FXML
	private TextField fname;
	@FXML
	private TextField lname;
	@FXML
	private TextField mname;
	@FXML
    private ComboBox<String> Vacancy;
   private String Select[]= {"Associative IT Manager","Junior Account Assistant","Payroll Administrator","Sales Representative","Senior QA Lead"};
	
	@FXML
	private TextField contactNo;
	@FXML
	private TextField email;
	@FXML
	private TextField resume;
	@FXML
	private TextField keywords;
	@FXML
	private TextField date;
	@FXML
	private Button save;
	@FXML
	private Button cancel;
	@FXML
	private Button admin;
	@FXML
	private Button recruitment;
	@FXML
	private Button candidates;
	@FXML
	private Button vacancies;
	
	public void save(ActionEvent event) {
		System.out.println(fname.getText());
		System.out.println(mname.getText());
		System.out.println(lname.getText());
		System.out.println(Vacancy.getValue());
		System.out.println(contactNo.getText());
		System.out.println(email.getText());
		System.out.println(resume.getText());
		System.out.println(keywords.getText());
		System.out.println(date.getText());
		if (fname.getText().isEmpty() || mname.getText().isEmpty() ||lname.getText().isEmpty() || Vacancy.getValue().isEmpty() ||contactNo.getText().isEmpty() || email.getText().isEmpty() ||resume.getText().isEmpty() || keywords.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText(null);
            alert.setContentText("Please fill all the Fields.");
            alert.showAndWait();
            return;
		}
		String query=" insert into candidates(FirstName,MiddleName,LastName,Vacancy,Email,ContactNo,Resume,Keywords,Date) values (\'"+fname.getText()+"',\'"+mname.getText()+"','"+lname.getText()+"','"+Vacancy.getValue()+"','"+email.getText()+"', '"+contactNo.getText()+"', '" +resume.getText()+"','" +keywords.getText()+"','"+date.getText()+"');";
		System.out.println(query);
		DbUtil.executeQuery(query);
		System.out.println("Event occur add controller "+event.getEventType().getName());
		new CandidateDashboard().Show();

	}
	public void AddCandidate(ActionEvent event) {
		new AddCandidate().Show();
	}
	public void admin(ActionEvent event) {
		new AddUser().Show();
	}
   public void Recruitment(ActionEvent event) {
		new CommonDashboard().Show();
	}
   public void AddVacancy(ActionEvent event) {
	  new AddVacancy().Show();
   }
   public void cancel(ActionEvent event) {
		new CandidateDashboard().Show();
	}
   @Override
  public void initialize(URL arg0, ResourceBundle arg1) {

	// TODO Auto-generated method stub
	Vacancy.getItems().addAll(Select);
	
}
   public void vacancy(ActionEvent event) {
	   
   }
  
	
}
