package dashboard;


import dashboard_candidate_recruitment.CommonDashboard;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import search_user.SearchUser;
import user_dashboard.UserDashboard;

public class DashboardController {
	@FXML
	private Button admin;
	@FXML
	private Button search;
	@FXML
	private Button recruitment;
	
	
	public void AddAdmin(ActionEvent event) {
		System.out.println("Addstart");
		new UserDashboard().Show();
		System.out.println("end");
	}
	public void search(ActionEvent event) {
		System.out.println("searchStart");
		new SearchUser().Show();
		System.out.println("end");
	}
	public void Recruitement(ActionEvent event) {
		System.out.println("recruitStart");
	  new CommonDashboard().Show();
	   System.out.println("end");
	}
	

}
