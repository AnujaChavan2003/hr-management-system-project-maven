package vacancy_dashboard;


import add_vacancy.AddVacancy;
import candidate_dashboard.CandidateDashboard;

import delete_vacancy.DeleteVacancy;

import edit_vacancy.EditVacancy;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

import search_vacancy.SearchVacancy;
import user_dashboard.UserDashboard;

public class VacancyDashboardController {

	@FXML
	private Button candidate;
	@FXML
	private Button search;
	@FXML
	private Button delete;
	@FXML
	private Button edit;
	@FXML
	private Button vacancy;
	@FXML
	private Button admin;
	
	
	public void Add(ActionEvent event) {
		new AddVacancy().Show();
	}

	public void Delete(ActionEvent event) {
		new DeleteVacancy().Show();
	}

	public void Search(ActionEvent event) {
		new SearchVacancy().Show();
	}

	public void Edit(ActionEvent event) {
		new EditVacancy().Show();
	}
	public void Candidate(ActionEvent event) {
		new CandidateDashboard().Show();
	}
	public void Vacancy(ActionEvent event) {
		new VacancyDashboard().Show();
	}
	public void Admin(ActionEvent event) {
		new UserDashboard().Show();
	}
	


	
}
