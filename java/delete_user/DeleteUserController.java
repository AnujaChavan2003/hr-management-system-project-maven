package delete_user;

import candidate_dashboard.CandidateDashboard;
import db_operation.DbUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import user_dashboard.UserDashboard;
import vacancy_dashboard.VacancyDashboard;


public class DeleteUserController {

	@FXML
	private Button admin;
	@FXML
	private Button candidate;
	@FXML
	private Button vacancy;
	@FXML
	private Button deleted;
	@FXML
	private Button cancel;
	@FXML
	private TextField empName;
	
	public void Admin(ActionEvent event) {
		new UserDashboard().Show();
	}
	public void Deleted(ActionEvent event) {
		System.out.println(empName.getText());
		if (empName.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText(null);
            alert.setContentText("Please fill the Field.");
            alert.showAndWait();
            return;
		}
		String query ="Delete from Users where UserName=\'"+empName.getText()+"';\r\n"
				+ "";
		System.out.println(query);
		DbUtil.executeQuery(query);
		System.out.println("Event occur delete controller " + event.getEventType().getName());
	    new UserDashboard().Show();
	}
	public void Cancel(ActionEvent event) {
		new UserDashboard().Show();
	}
	public void Vacancy(ActionEvent event) {
		new VacancyDashboard().Show();
	}
	public void Candidate(ActionEvent event) {
		new CandidateDashboard().Show();
	}
	
	
	
	

}
