package dashboard_candidate_recruitment;

import add_candidate.AddCandidate;
import add_user.AddUser;
import add_vacancy.AddVacancy;
import candidate_dashboard.CandidateDashboard;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import vacancy_dashboard.VacancyDashboard;

public class CommonDashboardController {

	@FXML
	private Button admin;
	@FXML
	private Button recruitment;
	@FXML
	private Button candidates;
	@FXML
	private Button vacancies;
	
	public void admin(ActionEvent event){
		new AddUser().Show();
	}
	public void recruitment(ActionEvent event)
	{
		new CommonDashboard().Show();
	}
	public void AddCandidate(ActionEvent event) {
		new CandidateDashboard().Show();
	}
	public void AddVacancy(ActionEvent event) {
		new VacancyDashboard().Show();
	}
	
	
}
