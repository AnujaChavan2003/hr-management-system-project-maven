package search_vacancy;

import javafx.beans.property.SimpleStringProperty;

public class User {

		public SimpleStringProperty vacancyName =new SimpleStringProperty();
		public SimpleStringProperty jobTitle=new SimpleStringProperty();
		public SimpleStringProperty hiringManager=new SimpleStringProperty();
		public SimpleStringProperty position=new SimpleStringProperty();
		

		  public String getVacancyName(){
			  return null!=vacancyName.get() ? vacancyName.get() : "";		   }

		   public String getJobTitle(){
			   return null!=jobTitle.get() ? jobTitle.get() : "";		   }
		   
		   public String getHiringManager(){
			   return null!=hiringManager.get() ? hiringManager.get() : "";		   }

		   public String getPosition(){
			   return null!=position.get() ? position.get() : "";		   }
	}


