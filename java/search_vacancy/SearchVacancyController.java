
package search_vacancy;

import java.net.URL;
import java.sql.ResultSet;
import java.util.ResourceBundle;

import candidate_dashboard.CandidateDashboard;
import db_operation.DbUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
	import javafx.collections.transformation.FilteredList;
	import javafx.collections.transformation.SortedList;
	import javafx.event.ActionEvent;
	import javafx.fxml.FXML;
	import javafx.fxml.Initializable;
	import javafx.scene.control.Button;
	import javafx.scene.control.TableColumn;
	import javafx.scene.control.TableView;
	import javafx.scene.control.TextField;
	import javafx.scene.control.cell.PropertyValueFactory;
import user_dashboard.UserDashboard;
import vacancy_dashboard.VacancyDashboard;

public class SearchVacancyController implements Initializable {
		@FXML
		private TextField vacancyToSearch;
		@FXML
		private Button cancel;
		@FXML
		private Button admin;
		@FXML
		private Button candidate;
		@FXML
		private Button vacancy;
		@FXML
		private TableView<User> tableView;
		@FXML

		private TableColumn<User, String> column1;
		@FXML
		private TableColumn<User, String> column2;
		@FXML
		private TableColumn<User, String> column3;
		@FXML
		private TableColumn<User, String> column4;
		
		private ObservableList<User> data;
	

		public void Admin(ActionEvent event) {

			new UserDashboard().Show();
		}		
		public void Cancel(ActionEvent event) {
			new VacancyDashboard().Show();

		}
		public void Candidate(ActionEvent event) {
			new CandidateDashboard().Show();

		}
		public void Vacancy(ActionEvent event) {
         new VacancyDashboard().Show();
		}

		@Override
		public void initialize(URL arg0, ResourceBundle arg1) {
			data = FXCollections.observableArrayList();

			column1.setCellValueFactory(new PropertyValueFactory<User, String>("vacancyName"));
			column2.setCellValueFactory(new PropertyValueFactory<User, String>("jobTitle"));
			column3.setCellValueFactory(new PropertyValueFactory<User, String>("hiringManager"));
			column4.setCellValueFactory(new PropertyValueFactory<User, String>("position"));
			
			buildData();

			FilteredList<User> filteredData = new FilteredList<>(data, b -> true);
			vacancyToSearch.textProperty().addListener((observable, oldValue, newValue) -> {
				filteredData.setPredicate(User -> {
					if (newValue.isEmpty() || newValue.isBlank() || newValue == null) {
						return true;
					}
					String searchKeyword = newValue.toLowerCase();
					if (User.getVacancyName().toLowerCase().indexOf(searchKeyword) > -1) {
						return true;
					} else if (User.getJobTitle().toLowerCase().indexOf(searchKeyword) > -1) {
						return true;
					} else if (User.getHiringManager().toLowerCase().indexOf(searchKeyword) > -1) {
						return true;
					} else if (User.getPosition().toLowerCase().indexOf(searchKeyword) > -1) {
						return true;
					}  else
						return false;

				});
			});

			SortedList<User> sortedData = new SortedList<>(filteredData);

			sortedData.comparatorProperty().bind(tableView.comparatorProperty());
			tableView.setItems(sortedData);

		}

		public void buildData() {
			try {
				data = FXCollections.observableArrayList();
				String query = "Select *from vacancy";
				System.out.println(query);
				ResultSet resultSet = DbUtil.executeQueryGetResult(query);
				while (resultSet.next()) {
					User user = new User();
					user.vacancyName.set(resultSet.getString(1));
					user.jobTitle.set(resultSet.getString(2));
					user.hiringManager.set(resultSet.getString(3));
					user.position.set(resultSet.getString(4));

					data.add(user);
				}
				tableView.setItems(data);

			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

	}

