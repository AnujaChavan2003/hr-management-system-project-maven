package edit_vacancy;

import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ResourceBundle;

import candidate_dashboard.CandidateDashboard;
import db_operation.DbUtil;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import user_dashboard.UserDashboard;
import vacancy_dashboard.VacancyDashboard;

public class EditVacancyController implements Initializable {

	@FXML
	private TextField vacancyToUpdate;
	@FXML
	private Label vacancyName;
	@FXML
	private TextField AddVacancy;
	@FXML
	private ChoiceBox<String> jobTitle;
	private String[] select = {"Chief Executive Officer", "Database Administrator","HR Manager","IT Manager","QA Lead","Social Media Marketer","Software Architect","Software Engineer"};
	String[] Select=select;
	
	@FXML
	private TextField hiringManager;
	@FXML
	private TextField position;
	@FXML
	private Button cancel;
	@FXML
	private Button save;
	@FXML
	private Button admin;
	@FXML
	private Button candidate;
	@FXML
	private Button vacancy;
	
	public void Cancel(ActionEvent event) {
		new VacancyDashboard().Show();
	}
    public void Save(ActionEvent event) throws SQLException {
    	System.out.println(vacancyToUpdate.getText());
    	System.out.println(AddVacancy.getText());
		System.out.println(jobTitle.getValue());
		System.out.println(hiringManager.getText());
		System.out.println(position.getText());
		String query1="select count(*) from vacancy where Vacancy='"+vacancyToUpdate.getText()+"'";
		 ResultSet rs1 =DbUtil.executeQueryGetResult(query1);
          rs1.next();
         if(rs1.getInt(1)==1) { 

		String query = "update vacancy set Vacancy='"+AddVacancy.getText()+"',jobTitle ='"+jobTitle.getValue()+"',hManager='"+hiringManager.getText()+"',position='"+position.getText()+"' where Vacancy='"+vacancyToUpdate.getText()+"';\r\n"
				+ "";
		 System.out.println(query);
			DbUtil.executeQuery(query);
			System.out.println("Event occur Edit controller "+event.getEventType().getName());
			new VacancyDashboard().Show();
			 } else {
				 Alert alert = new Alert(Alert.AlertType.ERROR);
					alert.setTitle("Error");
					alert.setHeaderText(null);
					alert.setContentText("Fill correct User");
					alert.showAndWait();
					return;
	         }
	
}
    public void Vacancy(ActionEvent event) {
    	new VacancyDashboard().Show();
       }
      public void Candidate(ActionEvent event) {
    	new CandidateDashboard().Show();
      }
       public void Admin(ActionEvent event) {
    	new UserDashboard().Show();
      }
       @Override
    	public void initialize(URL arg0, ResourceBundle arg1) {
    		// TODO Auto-generated method stub
    		jobTitle.getItems().addAll(select);

    	}
    	
    }

